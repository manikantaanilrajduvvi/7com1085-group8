# Search Protocol - Group 08

Search string

~~~~~ {.searchstring }
("insider attack*" OR "insider threat*" OR "data breach*") AND (financial OR bank* OR monetary OR commercial OR economic) AND (detect* OR Prevent*)
~~~~~

Number of papers: 56

Inclusion criteria:

1.The papers which explains identification, and prevention  of insider attacks mainly on financial or monetary organization is considered.

Exclusion criteria:

1.Papers which are not focusing on insider threat identification, detection or prevention and not associated with financial organization.


